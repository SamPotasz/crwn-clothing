export const SHOP_ACTION_TYPES = Object.freeze({
    GET_ITEMS_IN_CAT: 'get all items in a category',
})