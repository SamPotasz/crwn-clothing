//create an enum
export const UserActionTypes = Object.freeze({
    SET_CURR_USER: Symbol('set the current user'),
});